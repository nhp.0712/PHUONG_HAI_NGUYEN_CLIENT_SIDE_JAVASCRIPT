1. Create 3 pages – page1.html through page3.html a menu at the top that allows you to navigate between each of the 3 pages.

2.  Complete the following JavaScript exercises, 1 per page. All of your JavaScript code for each page will either go inside of a "script" tag or be linked using an external JavaScript file.

Page 1: Output a table of numbers from 5 to 15 including their squares and cubes.  These calculations must be done using JavaScript, the
Math.pow() function, and a loop of some sort.  The table used to display values must have 3 columns (Number, Square, and Cube), be well styled and centered.

Page 2: Output the first 20 Fibonacci numbers.  This sequence of numbers begins with 1, 1  and  the  next  number  is  the  sum  of  the  previous  two.   For  example,  the  third  number  in  the  se-  method along with the .join() function.

Page 3: Find a paragraph of text somewhere on the Internet (perhaps http://randomtextgenerator.com/ unique  word  in  the  text  in  alphabetical  order.   The  original  text  and  the  unique  words  should  be displayed as shown below (The headings “Original Text” and “Unique Words” must be "h2" tags).